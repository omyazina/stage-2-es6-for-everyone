import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';
import App from '../../app';

export function showWinnerModal(fighter) {
  const title = `${fighter.name}, You win!!`;
  const bodyElement = createBodyElement();

  showModal({ title, bodyElement, onClose: () => {
    const root = document.getElementById('root');

    root.innerHTML = '';
    new App();
  } });
}

function createBodyElement() {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  bodyElement.append("Press <close> to show main menu.");

  return bodyElement;
}