import { controls } from '../../constants/controls';
import { runOnKeys } from '../helpers/keysHelper';
import { getFighterSelector, getHealthIndicatorSelector, getEnergyIndicatorSelector } from './arena';

let fightersState = {};

export async function fight(firstFighter, secondFighter) {
  initState(firstFighter, secondFighter);

  setFighterBlockKey(fightersState.first, controls.PlayerOneBlock);
  setFighterBlockKey(fightersState.second, controls.PlayerTwoBlock);

  return new Promise((resolve) => {

    runOnKeys(
      () => {
        if (!fightersState.second.isBlocked && !fightersState.first.isBlocked) {
          const attacker = fightersState.first;
          const defender = fightersState.second;

          attack(attacker, defender, false, resolve);
        }
      },
      controls.PlayerOneAttack
    );

    runOnKeys(
      () => {
        if (!fightersState.first.isBlocked && !fightersState.second.isBlocked) {          
          const attacker = fightersState.second;
          const defender = fightersState.first;

          attack(attacker, defender, false, resolve);
        }
      },
      controls.PlayerTwoAttack
    );

    runOnKeys(
      () => {
        if (!fightersState.first.isBlocked) {
          const attacker = fightersState.first;
          const defender = fightersState.second;

          attack(attacker, defender, true, resolve);
        }
      },
      ...controls.PlayerOneCriticalHitCombination
    );

    runOnKeys(
      () => {
        if (!fightersState.second.isBlocked) {
          const attacker = fightersState.second;
          const defender = fightersState.first;

          attack(attacker, defender, true, resolve);
        }
      },
      ...controls.PlayerTwoCriticalHitCombination
    );
  });
}

function initState(firstFighter, secondFighter) {  
  fightersState = { timeOfLastCritical: {} };
  fightersState.first = Object.assign({position: 'left'}, firstFighter);
  fightersState.second = Object.assign({position: 'right'}, secondFighter);

  fightersState.first.healthRemain = fightersState.first.health;
  fightersState.second.healthRemain = fightersState.second.health;
}

function setFighterBlockKey(fighgter, keyCode) {
  document.addEventListener('keydown', function(event) {
    if (event.code === keyCode) {
      fighgter.isBlocked = true;
    }
  });

  document.addEventListener('keyup', function(event) {
    if (event.code === keyCode) {
      fighgter.isBlocked = false;
    }
  });
}

function animateFighter(fighter, cssClass, duration) {
  getFighterSelector(fighter.position).classList.add(cssClass);
  setTimeout(() => {
    getFighterSelector(fighter.position).classList.remove(cssClass);
  }, duration);
}

function attack(attacker, defender, isCritical, resolve) {
  let damage;

  if (isCritical) {
    damage = getCriticalDamage(attacker);

    if (damage > 0) {
      animateEnergyIndicator(attacker, '--empty', 200);
      animateFighter(attacker, '--criticalAttack', 120);
    }
  } else {
    animateFighter(attacker, '--attack', 120);
    damage = getDamage(attacker, defender);
  }

  defender.healthRemain -= damage;
  updateHealthIndicators();

  if (defender.healthRemain <= 0) {
    killFighter(defender);
    resolve(attacker);
  }
}

function updateHealthIndicators() {
  updateHealthIndicator(fightersState.first);
  updateHealthIndicator(fightersState.second);
}

function updateHealthIndicator(fighter) {
  const fighterIndicator = getHealthIndicatorSelector(fighter.position);

  let fighterHealthRelative = fighter.healthRemain / fighter.health * 100;
  fighterHealthRelative = fighterHealthRelative > 0 ? fighterHealthRelative : 0;

  fighterIndicator.style.width = fighterHealthRelative + '%';

  if (fighterHealthRelative < 30) {
    fighterIndicator.classList.add('--diying');
  }
}

export function getCriticalDamage(attacker) {
  const minTimeMs = 10 * 1000;
  const timeOfLastCritical = fightersState.timeOfLastCritical[`${attacker.position}`];

  if (timeOfLastCritical && (new Date() - timeOfLastCritical < minTimeMs)) {
    return 0;
  }

  fightersState.timeOfLastCritical[`${attacker.position}`] = new Date();

  return attacker.attack * 2;
}

function animateEnergyIndicator(fighter, cssClass, duration) {
  getEnergyIndicatorSelector(fighter.position).classList.add(cssClass);
  setTimeout(() => {
    getEnergyIndicatorSelector(fighter.position).classList.remove(cssClass);
  }, duration);
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  damage = damage > 0 ? damage : 0;
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}

function killFighter(fighter) {
  getFighterSelector(fighter.position).classList.add('--killed');
}